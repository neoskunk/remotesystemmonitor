﻿using RemoteSystemMonitor.Properties;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace RemoteSystemMonitor
{
    internal static class RemoteSystemMonitorSettings
    {
        private static readonly JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
        private static string[] _split = new string[] { "," };
        private static IDictionary<string, IDictionary<string, string>> _systemsList = _jsonSerializer.Deserialize<IDictionary<string, IDictionary<string, string>>>(Settings.Default.SystemsList);

        internal static IEnumerable<string> GetSystemsList(bool includeLocalhost = false)
        {
            if (!includeLocalhost)
            {
                return _systemsList.Keys.Where(x => x != Constant.Localhost).OrderBy(x => x == Constant.Localhost ? 0 : 1).ThenBy(x => x);
            }

            return _systemsList.Keys.OrderBy(x => x).OrderBy(x => x == Constant.Localhost ? 0 : 1).ThenBy(x => x);
        }

        internal static bool SetSystemsList(string[] systems)
        {
            if (systems.Length == _systemsList.Keys.Count - 1 && systems.All(x => _systemsList.ContainsKey(x)))
            {
                return false;
            }

            var newSystemsList = new Dictionary<string, IDictionary<string, string>>();
            newSystemsList.Add(Constant.Localhost, _systemsList[Constant.Localhost]);

            systems.Where(x => _systemsList.ContainsKey(x)).ToList().ForEach(x => newSystemsList.Add(x, _systemsList[x]));
            systems.Where(x => !_systemsList.ContainsKey(x)).ToList().ForEach(x => newSystemsList.Add(x, GetEmptyDiskAndNetworkDictionary()));

            Settings.Default.SystemsList = _jsonSerializer.Serialize(newSystemsList);

            if (!systems.Contains(GetSystemName()))
            {
                Settings.Default.SystemName = Constant.Localhost;
            }

            Settings.Default.Save();

            _systemsList = newSystemsList;

            return true;
        }

        internal static string GetSystemName() => Settings.Default.SystemName;

        internal static void SetSystemName(string system)
        {
            Settings.Default.SystemName = system;

            Settings.Default.Save();
        }

        internal static string GetDiskInstanceName(string systemName) => _systemsList[systemName][Constant.DiskHistory];

        internal static void SetDiskInstanceName(string diskInstanceName)
        {
            _systemsList[GetSystemName()][Constant.DiskHistory] = diskInstanceName;

            Settings.Default.SystemsList = _jsonSerializer.Serialize(_systemsList);

            Settings.Default.Save();
        }

        internal static string GetNetworkInstanceName(string systemName) => _systemsList[systemName][Constant.NetworkHistory];

        internal static void SetNetworkInstanceName(string networkInstanceName)
        {
            _systemsList[GetSystemName()][Constant.NetworkHistory] = networkInstanceName;

            Settings.Default.SystemsList = _jsonSerializer.Serialize(_systemsList);

            Settings.Default.Save();
        }

        internal static void SetSystem(string systemName, string diskInstanceName, string networkInstanceName)
        {
            Settings.Default.SystemName = systemName;

            _systemsList[systemName][Constant.DiskHistory] = diskInstanceName;
            _systemsList[systemName][Constant.NetworkHistory] = networkInstanceName;

            Settings.Default.SystemsList = _jsonSerializer.Serialize(_systemsList);

            Settings.Default.Save();
        }

        private static IDictionary<string, string> GetEmptyDiskAndNetworkDictionary()
        {
            return new Dictionary<string, string>()
            {
                { Constant.DiskHistory, null },
                { Constant.NetworkHistory, null }
            };
        }
    }
}
