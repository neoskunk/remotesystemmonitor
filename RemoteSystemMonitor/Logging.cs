﻿using System;
using System.Diagnostics;
using System.Text;

namespace RemoteSystemMonitor
{
    internal static class Logging
    {
        private static readonly DateTime _programStartTime = DateTime.Now;

        private static DateTime _cpuMonitorStartTime, _memoryMonitorStartTime, _diskMonitorStartTime, _networkMonitorStartTime;

        internal static void SetProgramStartTime() { }

        internal static void SetAllMonitorStartTimes()
        {
            _cpuMonitorStartTime = DateTime.Now;
            _memoryMonitorStartTime = _cpuMonitorStartTime;
            _diskMonitorStartTime = _cpuMonitorStartTime;
            _networkMonitorStartTime = _cpuMonitorStartTime;
        }

        internal static void SetDiskMonitorStartTime() => _diskMonitorStartTime = DateTime.Now;

        internal static void SetNetworkMonitorStartTime() => _networkMonitorStartTime = DateTime.Now;

        internal static void WriteExceptionToEventLog(Type objectTypeWithException, Exception exception)
        {
            if (!EventLog.SourceExists(Constant.EventSource))
            {
                EventLog.CreateEventSource(Constant.EventSource, Constant.EventLogName);
            }

            EventLog.WriteEntry(Constant.EventSource, string.Format("Exception in {0}{1}{1}{2}{1}{1}{3}", objectTypeWithException.Name, Environment.NewLine, GetDetailedMessageFromException(exception), GetRunningTimeDetails()), EventLogEntryType.Error);
        }

        private static string GetDetailedMessageFromException(Exception exception)
        {
            var result = new StringBuilder();

            result.AppendFormat("{0}{1}{2}", exception.Message, Environment.NewLine, exception.StackTrace);

            while (exception.InnerException != null)
            {
                exception = exception.InnerException;

                result.AppendFormat("{0}{0}{1}{0}{2}", Environment.NewLine, exception.Message, exception.StackTrace);
            }

            return result.ToString();
        }

        private static string GetRunningTimeDetails()
        {
            var dateTimeNow = DateTime.Now;

            var runningTime = dateTimeNow - _programStartTime;

            var result = new StringBuilder();

            result.AppendFormat("Program began execution on {0} at {1} (Running for {2}){3}", _programStartTime.ToShortDateString(), _programStartTime.ToShortTimeString(), runningTime.ToPrettyFormat(), Environment.NewLine);

            result.AppendMonitorTimeSpanFormat(_cpuMonitorStartTime, dateTimeNow, "CPU");
            result.AppendMonitorTimeSpanFormat(_memoryMonitorStartTime, dateTimeNow, "Memory");
            result.AppendMonitorTimeSpanFormat(_diskMonitorStartTime, dateTimeNow, "Disk");
            result.AppendMonitorTimeSpanFormat(_networkMonitorStartTime, dateTimeNow, "Network");

            return result.ToString();
        }

        private static string ToPrettyFormat(this TimeSpan timeSpan)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendTimeSpanFormat(timeSpan.Days, "day");
            stringBuilder.AppendTimeSpanFormat(timeSpan.Hours, "hour");
            stringBuilder.AppendTimeSpanFormat(timeSpan.Minutes, "minute");
            stringBuilder.AppendTimeSpanFormat(timeSpan.Seconds, "second");

            return stringBuilder.ToString().Trim();
        }

        private static void AppendTimeSpanFormat(this StringBuilder stringBuilder, int span, string label)
        {
            if (span > 0)
            {
                stringBuilder.AppendFormat("{0} {1}{2} ", span, label, span > 1 ? "s" : String.Empty);
            }
        }

        private static void AppendMonitorTimeSpanFormat(this StringBuilder stringBuilder, DateTime startTime, DateTime now, string label)
        {
            if (startTime != null)
            {
                var timeSpan = now - startTime;

                stringBuilder.AppendFormat("{0}{1} monitor connected on {2} at {3} (Connected for {4})", Environment.NewLine, label, startTime.ToShortDateString(), startTime.ToShortTimeString(), timeSpan.ToPrettyFormat());
            }
        }
    }
}
