﻿namespace RemoteSystemMonitor
{
    internal static class Constant
    {
        internal const string CPUChartArea = "CPU";
        internal const string MemoryChartArea = "Memory";
        internal const string DiskChartArea = "Disk";
        internal const string NetworkChartArea = "Network";

        internal const string SystemsToolStripMenuItem = "systemsToolStripMenuItem";
        internal const string DiskToolStripMenuItem = "diskToolStripMenuItem";
        internal const string NetworkToolStripMenuItem = "networkToolStripMenuItem";

        internal const string CPUTotalSeries = "CPUTotal";
        internal const string MemoryTotalSeries = "MemoryTotal";
        internal const string DiskReadSeries = "DiskRead";
        internal const string DiskWriteSeries = "DiskWrite";
        internal const string NetworkReceiveSeries = "NetworkReceive";
        internal const string NetworkSendSeries = "NetworkSend";

        internal const string ProcessorCategoryName = "Processor";
        internal const string DiskCategoryName = "PhysicalDisk";
        internal const string MemoryCategoryName = "Memory";
        internal const string NetworkCategoryName = "Network Interface";

        internal const string ProcessorPercentTimeCounterName = "% Processor Time";
        internal const string DiskReadBytesPerSecCounterName = "Disk Read Bytes/sec";
        internal const string DiskWriteBytesPerSecCountername = "Disk Write Bytes/sec";
        internal const string MemoryAvailableBytesCounterName = "Available Bytes";
        internal const string NetworkSendBytesPerSecCounterName = "Bytes Sent/sec";
        internal const string NetworkReceiveBytesPerSecCounterName = "Bytes Received/sec";

        internal const string _Total = "_Total";

        internal const string Localhost = "localhost";
        internal const string DiskHistory = "disk";
        internal const string NetworkHistory = "network";

        internal const string EventSource = "Remote System Monitor";
        internal const string EventLogName = "Application";
    }
}
