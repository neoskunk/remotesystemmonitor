﻿using System.Drawing;
using System.Windows.Forms;

namespace RemoteSystemMonitor.Controls
{
    internal class MenuStripColorTable : ProfessionalColorTable
    {
        public override Color MenuItemSelected
        {
            get { return Color.Black; }
        }

        public override Color MenuItemBorder
        {
            get { return Color.White; }
        }

        public override Color MenuItemSelectedGradientBegin
        {
            get { return Color.Black; }
        }

        public override Color MenuItemSelectedGradientEnd
        {
            get { return Color.Black; }
        }

        public override Color MenuItemPressedGradientBegin
        {
            get { return Color.Black; }
        }

        public override Color MenuItemPressedGradientMiddle
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemPressedGradientEnd
        {
            get { return Color.DimGray; }
        }

        public override Color ToolStripDropDownBackground
        {
            get { return Color.DimGray; }
        }
    }
}
