﻿namespace RemoteSystemMonitor
{
    partial class EditSystemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.addSystemsGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.addSystemsTextBox = new System.Windows.Forms.TextBox();
            this.addSystemsButton = new System.Windows.Forms.Button();
            this.removeSystemsGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.systemsListBox = new System.Windows.Forms.ListBox();
            this.removeSystemsButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.addSystemsGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.removeSystemsGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.addSystemsGroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.removeSystemsGroupBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cancelButton, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.acceptButton, 2, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(374, 251);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // addSystemsGroupBox
            // 
            this.addSystemsGroupBox.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.addSystemsGroupBox, 3);
            this.addSystemsGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.addSystemsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addSystemsGroupBox.ForeColor = System.Drawing.Color.White;
            this.addSystemsGroupBox.Location = new System.Drawing.Point(3, 3);
            this.addSystemsGroupBox.Name = "addSystemsGroupBox";
            this.addSystemsGroupBox.Size = new System.Drawing.Size(368, 49);
            this.addSystemsGroupBox.TabIndex = 0;
            this.addSystemsGroupBox.TabStop = false;
            this.addSystemsGroupBox.Text = "Add System(s)";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.addSystemsTextBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.addSystemsButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(362, 30);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // addSystemsTextBox
            // 
            this.addSystemsTextBox.BackColor = System.Drawing.Color.Black;
            this.addSystemsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addSystemsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSystemsTextBox.ForeColor = System.Drawing.Color.White;
            this.addSystemsTextBox.Location = new System.Drawing.Point(3, 3);
            this.addSystemsTextBox.Name = "addSystemsTextBox";
            this.addSystemsTextBox.Size = new System.Drawing.Size(275, 24);
            this.addSystemsTextBox.TabIndex = 0;
            // 
            // addSystemsButton
            // 
            this.addSystemsButton.ForeColor = System.Drawing.Color.Black;
            this.addSystemsButton.Location = new System.Drawing.Point(284, 3);
            this.addSystemsButton.Name = "addSystemsButton";
            this.addSystemsButton.Size = new System.Drawing.Size(75, 23);
            this.addSystemsButton.TabIndex = 1;
            this.addSystemsButton.Text = "Add";
            this.addSystemsButton.UseVisualStyleBackColor = true;
            this.addSystemsButton.Click += new System.EventHandler(this.addSystemsButton_Click);
            // 
            // removeSystemsGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.removeSystemsGroupBox, 3);
            this.removeSystemsGroupBox.Controls.Add(this.tableLayoutPanel3);
            this.removeSystemsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removeSystemsGroupBox.ForeColor = System.Drawing.Color.White;
            this.removeSystemsGroupBox.Location = new System.Drawing.Point(3, 58);
            this.removeSystemsGroupBox.Name = "removeSystemsGroupBox";
            this.removeSystemsGroupBox.Size = new System.Drawing.Size(368, 161);
            this.removeSystemsGroupBox.TabIndex = 1;
            this.removeSystemsGroupBox.TabStop = false;
            this.removeSystemsGroupBox.Text = "Remove System(s)";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.systemsListBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.removeSystemsButton, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(362, 142);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // systemsListBox
            // 
            this.systemsListBox.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel3.SetColumnSpan(this.systemsListBox, 3);
            this.systemsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemsListBox.ForeColor = System.Drawing.Color.White;
            this.systemsListBox.FormattingEnabled = true;
            this.systemsListBox.IntegralHeight = false;
            this.systemsListBox.Location = new System.Drawing.Point(3, 3);
            this.systemsListBox.Name = "systemsListBox";
            this.systemsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.systemsListBox.Size = new System.Drawing.Size(356, 107);
            this.systemsListBox.TabIndex = 0;
            // 
            // removeSystemsButton
            // 
            this.removeSystemsButton.ForeColor = System.Drawing.Color.Black;
            this.removeSystemsButton.Location = new System.Drawing.Point(143, 116);
            this.removeSystemsButton.Name = "removeSystemsButton";
            this.removeSystemsButton.Size = new System.Drawing.Size(75, 23);
            this.removeSystemsButton.TabIndex = 1;
            this.removeSystemsButton.Text = "Remove";
            this.removeSystemsButton.UseVisualStyleBackColor = true;
            this.removeSystemsButton.Click += new System.EventHandler(this.removeSystemsButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(215, 225);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(296, 225);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 3;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // EditSystemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "EditSystemsForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Edit Systems";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.addSystemsGroupBox.ResumeLayout(false);
            this.addSystemsGroupBox.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.removeSystemsGroupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox addSystemsGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox addSystemsTextBox;
        private System.Windows.Forms.Button addSystemsButton;
        private System.Windows.Forms.GroupBox removeSystemsGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListBox systemsListBox;
        private System.Windows.Forms.Button removeSystemsButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button acceptButton;



    }
}