﻿using RemoteSystemMonitor.Models;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Management;

namespace RemoteSystemMonitor.Monitors
{
    internal class MemoryMonitor
    {
        private readonly PerformanceCounter _counter;
        private readonly float _totalMemoryInGigabytes;

        internal float TotalMemoryInGigabytes { get { return _totalMemoryInGigabytes; } }

        internal MemoryMonitor(string systemName)
        {
            _counter = new PerformanceCounter(Constant.MemoryCategoryName, Constant.MemoryAvailableBytesCounterName, string.Empty, systemName);
            _totalMemoryInGigabytes = GetAndSetTotalMemory(systemName);

            // first performance counter value is inaccurate
            _counter.NextValue();
        }

        internal MemoryData GetMemoryData() => new MemoryData(_totalMemoryInGigabytes - Shift.BytesToGigabytes(_counter.NextValue()));

        private float GetAndSetTotalMemory(string systemName)
        {
            var managementScope = new ManagementScope($"\\\\{systemName}\\root\\CIMV2");
            var objectQuery = new ObjectQuery("select TotalPhysicalMemory from Win32_ComputerSystem");
            var managementObjectSearcher = new ManagementObjectSearcher(managementScope, objectQuery);

            UInt64 physicalMemory = 0;

            foreach (ManagementObject o in managementObjectSearcher.Get())
            {
                physicalMemory += Convert.ToUInt64(o["TotalPhysicalMemory"], CultureInfo.InvariantCulture);
            }

            return Shift.BytesToGigabytes(physicalMemory);
        }
    }
}
