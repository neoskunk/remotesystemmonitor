﻿using RemoteSystemMonitor.Models;
using System.Diagnostics;

namespace RemoteSystemMonitor.Monitors
{
    internal class NetworkMonitor
    {
        private readonly PerformanceCounter _sendCounter, _receiveCounter;
        private readonly string _instanceName;

        internal NetworkMonitor(string systemName, string instanceName)
        {
            _instanceName = instanceName;

            _sendCounter = new PerformanceCounter(Constant.NetworkCategoryName, Constant.NetworkSendBytesPerSecCounterName, instanceName, systemName);
            _receiveCounter = new PerformanceCounter(Constant.NetworkCategoryName, Constant.NetworkReceiveBytesPerSecCounterName, instanceName, systemName);

            // first performance counter value is inaccurate
            _sendCounter.NextValue();
            _receiveCounter.NextValue();
        }

        internal NetworkData GetNetworkData() => new NetworkData(Shift.BytesToMegabytes(_sendCounter.NextValue()), Shift.BytesToMegabytes(_receiveCounter.NextValue()));

        internal string GetInstanceName() => _instanceName;
    }
}
