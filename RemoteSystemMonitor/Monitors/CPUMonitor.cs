﻿using RemoteSystemMonitor.Models;
using System.Diagnostics;

namespace RemoteSystemMonitor.Monitors
{
    internal class CPUMonitor
    {
        private readonly PerformanceCounter _counter;

        internal CPUMonitor(string systemName)
        {
            _counter = new PerformanceCounter(Constant.ProcessorCategoryName, Constant.ProcessorPercentTimeCounterName, Constant._Total, systemName);

            // first performance counter value is inaccurate
            _counter.NextValue();
        }

        internal CPUData GetCPUData() => new CPUData(_counter.NextValue());
    }
}
