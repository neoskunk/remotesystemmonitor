﻿using RemoteSystemMonitor.Models;
using System.Diagnostics;

namespace RemoteSystemMonitor.Monitors
{
    internal class DiskMonitor
    {
        private readonly PerformanceCounter _readCounter, _writeCounter;
        private readonly string _instanceName;

        internal DiskMonitor(string systemName, string instanceName)
        {
            _instanceName = instanceName;

            _readCounter = new PerformanceCounter(Constant.DiskCategoryName, Constant.DiskReadBytesPerSecCounterName, instanceName, systemName);
            _writeCounter = new PerformanceCounter(Constant.DiskCategoryName, Constant.DiskWriteBytesPerSecCountername, instanceName, systemName);

            // first performance counter value is inaccurate
            _readCounter.NextValue();
            _writeCounter.NextValue();
        }

        internal DiskData GetDiskData() => new DiskData(Shift.BytesToMegabytes(_readCounter.NextValue()), Shift.BytesToMegabytes(_writeCounter.NextValue()));

        internal string GetInstanceName() => _instanceName;
    }
}
