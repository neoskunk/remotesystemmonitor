﻿using RemoteSystemMonitor.Models;

namespace RemoteSystemMonitor.Monitors
{
    internal static class SystemMonitor
    {
        private static CPUMonitor _cpuMonitor;
        private static MemoryMonitor _memoryMonitor;
        private static DiskMonitor _diskMonitor;
        private static NetworkMonitor _networkMonitor;

        internal static void SetMonitors(CPUMonitor cpuMonitor, MemoryMonitor memoryMonitor, DiskMonitor diskMonitor, NetworkMonitor networkMonitor)
        {
            _cpuMonitor = cpuMonitor;
            _memoryMonitor = memoryMonitor;
            _diskMonitor = diskMonitor;
            _networkMonitor = networkMonitor;
        }

        internal static void SetDiskMonitor(DiskMonitor diskMonitor) => _diskMonitor = diskMonitor;

        internal static void SetNetworkMonitor(NetworkMonitor networkMonitor) => _networkMonitor = networkMonitor;

        internal static SystemData GetSystemData() => new SystemData(_cpuMonitor.GetCPUData(), _memoryMonitor.GetMemoryData(), _diskMonitor.GetDiskData(), _networkMonitor.GetNetworkData());

        internal static float GetTotalMemoryInGigabytes() => _memoryMonitor.TotalMemoryInGigabytes;
    }
}
