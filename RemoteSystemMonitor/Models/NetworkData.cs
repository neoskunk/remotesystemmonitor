﻿namespace RemoteSystemMonitor.Models
{
    internal class NetworkData
    {
        internal float SendInMegabytesPerSecond { get; private set; }
        internal float ReceiveInMegabytesPerSecond { get; private set; }

        internal NetworkData(float sendInMegabytesPerSecond, float receiveInMegabytesPerSecond)
        {
            SendInMegabytesPerSecond = sendInMegabytesPerSecond;
            ReceiveInMegabytesPerSecond = receiveInMegabytesPerSecond;
        }
    }
}
