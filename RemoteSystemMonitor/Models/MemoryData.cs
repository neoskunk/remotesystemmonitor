﻿namespace RemoteSystemMonitor.Models
{
    internal class MemoryData
    {
        internal float TotalInGigabytes { get; private set; }

        internal MemoryData(float totalInGigabytes)
        {
            TotalInGigabytes = totalInGigabytes;
        }
    }
}
