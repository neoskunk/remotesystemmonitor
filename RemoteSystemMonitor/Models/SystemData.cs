﻿namespace RemoteSystemMonitor.Models
{
    internal class SystemData
    {
        internal CPUData CPUData { get; private set; }
        internal MemoryData MemoryData { get; private set; }
        internal DiskData DiskData { get; private set; }
        internal NetworkData NetworkData { get; private set; }

        internal SystemData(CPUData cpuData, MemoryData memoryData, DiskData diskData, NetworkData networkData)
        {
            CPUData = cpuData;
            MemoryData = memoryData;
            DiskData = diskData;
            NetworkData = networkData;
        }
    }
}
