﻿namespace RemoteSystemMonitor.Models
{
    internal class CPUData
    {
        internal float TotalPercent { get; private set; }

        internal CPUData(float totalPercent)
        {
            TotalPercent = totalPercent;
        }
    }
}
