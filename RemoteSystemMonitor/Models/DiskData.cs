﻿namespace RemoteSystemMonitor.Models
{
    internal class DiskData
    {
        internal float ReadInMegabytesPerSecond { get; private set; }
        internal float WriteInMegabytesPerSecond { get; private set; }

        internal DiskData(float readInMegabytesPerSecond, float writeInMegabytesPerSecond)
        {
            ReadInMegabytesPerSecond = readInMegabytesPerSecond;
            WriteInMegabytesPerSecond = writeInMegabytesPerSecond;
        }
    }
}
