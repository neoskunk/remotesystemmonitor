﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RemoteSystemMonitor
{
    internal partial class EditSystemsForm : Form
    {
        internal bool SystemsListChanged { get; set; }

        internal EditSystemsForm()
        {
            InitializeComponent();

            ActiveControl = addSystemsTextBox;

            addSystemsTextBox.KeyPress += addSystemsTextBox_KeyPress;

            SetSystemsListInListBox(RemoteSystemMonitorSettings.GetSystemsList());
        }

        private void TryAddSystem(string system)
        {
            if (!string.IsNullOrWhiteSpace(system) && !systemsListBox.Items.Contains(system.ToLowerInvariant()))
            {
                systemsListBox.Items.Add(system.ToLowerInvariant());

                addSystemsTextBox.Text = string.Empty;
            }
        }

        private void addSystemsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                TryAddSystem(addSystemsTextBox.Text);

                e.Handled = true;
            }
        }

        private void addSystemsButton_Click(object sender, EventArgs e) => TryAddSystem(addSystemsTextBox.Text);

        private void removeSystemsButton_Click(object sender, EventArgs e)
        {
            systemsListBox.BeginUpdate();

            for (var i = systemsListBox.SelectedIndices.Count - 1; i >= 0; i--)
            {
                systemsListBox.Items.RemoveAt(systemsListBox.SelectedIndices[i]);
            }

            systemsListBox.EndUpdate();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            SystemsListChanged = RemoteSystemMonitorSettings.SetSystemsList(GetSystemsListFromListBox());

            Close();
        }

        private string[] GetSystemsListFromListBox()
        {
            var result = new List<string>();

            foreach (var item in systemsListBox.Items)
            {
                result.Add(item.ToString());
            }

            return result.ToArray();
        }

        private void SetSystemsListInListBox(IEnumerable<string> systems)
        {
            foreach (var system in systems)
            {
                systemsListBox.Items.Add(system);
            }
        }
    }
}
