﻿namespace RemoteSystemMonitor
{
    internal static class Shift
    {
        internal static float BytesToKilobytes(float bytes) => bytes / 1024f;

        internal static float BytesToMegabytes(float bytes) => bytes / 1048576f;

        internal static float BytesToGigabytes(float bytes) => bytes / 1073741824f;
    }
}
