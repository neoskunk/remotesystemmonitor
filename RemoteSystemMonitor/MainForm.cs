﻿using RemoteSystemMonitor.BackgroundWorkers;
using RemoteSystemMonitor.Controls;
using RemoteSystemMonitor.Models;
using RemoteSystemMonitor.Monitors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace RemoteSystemMonitor
{
    internal partial class MainForm : Form
    {
        private BackgroundWorker _changeSystemBackgroundWorker, _changeDiskInstanceBackgroundWorker, _changeNetworkInstanceBackgroundWorker, _updateChartBackgroundWorker, _delayedActionBackgroundWorker;

        internal MainForm()
        {
            InitializeComponent();

            menuStrip.Renderer = new ToolStripProfessionalRenderer(new MenuStripColorTable());

            chart.ChartAreas[Constant.CPUChartArea].AxisY.Minimum = 0;
            chart.ChartAreas[Constant.CPUChartArea].AxisY.Maximum = 100;
            chart.ChartAreas[Constant.CPUChartArea].AxisX.Minimum = 1;
            chart.ChartAreas[Constant.CPUChartArea].AxisX.Maximum = 61;

            chart.ChartAreas[Constant.MemoryChartArea].AxisY.Minimum = 0;
            chart.ChartAreas[Constant.MemoryChartArea].AxisX.Minimum = 1;
            chart.ChartAreas[Constant.MemoryChartArea].AxisX.Maximum = 61;

            chart.ChartAreas[Constant.DiskChartArea].AxisX.Minimum = 1;
            chart.ChartAreas[Constant.DiskChartArea].AxisX.Maximum = 61;

            chart.ChartAreas[Constant.NetworkChartArea].AxisX.Minimum = 1;
            chart.ChartAreas[Constant.NetworkChartArea].AxisX.Maximum = 61;

            FormClosing += MainForm_FormClosing;

            UpdateSystemsMenu();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _changeDiskInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeNetworkInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeSystemBackgroundWorker.CancelIfNotNullAndBusy();
            _updateChartBackgroundWorker.CancelIfNotNullAndBusy();
            _delayedActionBackgroundWorker.CancelIfNotNullAndBusy();
        }

        private void systemsRadioButton_OnCheckedChanged(object sender, EventArgs e)
        {
            var radioButton = sender as ToolStripRadioButtonMenuItem;

            if (radioButton.Checked && radioButton.Text != ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).Text)
            {
                SetSystem(radioButton.Text);
            }
        }

        private void diskRadioButton_OnCheckedChanged(object sender, EventArgs e)
        {
            var radioButton = sender as ToolStripRadioButtonMenuItem;

            if (radioButton.Checked)
            {
                SetDiskInstance(radioButton.Text);
            }
        }

        private void networkRadioButton_OnCheckedChanged(object sender, EventArgs e)
        {
            var radioButton = sender as ToolStripRadioButtonMenuItem;

            if (radioButton.Checked)
            {
                SetNetworkInstance(radioButton.Text);
            }
        }

        private void editSystemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var editSystemsForm = new EditSystemsForm();

            editSystemsForm.ShowDialog();

            if (editSystemsForm.SystemsListChanged)
            {
                UpdateSystemsMenu();
            }
        }

        private void UpdateSystemsMenu()
        {
            ToolStripItem[] systemMenuItems = new ToolStripItem[((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).DropDownItems.Count];
            ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).DropDownItems.CopyTo(systemMenuItems, 0);

            ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).DropDownItems.Clear();

            ToolStripRadioButtonMenuItem[] systemOptions;

            var checkedSystem = systemMenuItems.FirstOrDefault(x => x is ToolStripRadioButtonMenuItem && ((ToolStripRadioButtonMenuItem)x).Checked);

            if (checkedSystem != null && checkedSystem.Text == RemoteSystemMonitorSettings.GetSystemName())
            {
                systemOptions = RemoteSystemMonitorSettings.GetSystemsList(true)
                    .Select(x => new ToolStripRadioButtonMenuItem(x, systemsRadioButton_OnCheckedChanged, x == RemoteSystemMonitorSettings.GetSystemName()))
                    .ToArray();
            }
            else
            {
                systemOptions = RemoteSystemMonitorSettings.GetSystemsList(true)
                    .Select(x => new ToolStripRadioButtonMenuItem(x, systemsRadioButton_OnCheckedChanged))
                    .ToArray();

                systemOptions.First(x => x.Text == RemoteSystemMonitorSettings.GetSystemName()).Checked = true;
            }

            var newSystemMenuItems = new List<ToolStripItem>();
            newSystemMenuItems.AddRange(systemOptions);
            newSystemMenuItems.Add(systemMenuItems[systemMenuItems.Length - 2]);
            newSystemMenuItems.Add(systemMenuItems[systemMenuItems.Length - 1]);

            ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).DropDownItems.AddRange(newSystemMenuItems.ToArray());
        }

        private void SetSystem(string systemName)
        {
            toolStripStatusLabel.Text = $"Connecting to {systemName}...";
            toolStripStatusLabel.Visible = true;
            toolStripProgressBar.Visible = true;

            ((ToolStripMenuItem)menuStrip.Items[Constant.DiskToolStripMenuItem]).Visible = false;
            ((ToolStripMenuItem)menuStrip.Items[Constant.NetworkToolStripMenuItem]).Visible = false;

            _changeDiskInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeNetworkInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeSystemBackgroundWorker.CancelIfNotNullAndBusy();
            _delayedActionBackgroundWorker.CancelIfNotNullAndBusy();

            _changeSystemBackgroundWorker = new ChangeSystemBackgroundWorker(this, systemName, RemoteSystemMonitorSettings.GetDiskInstanceName(systemName), RemoteSystemMonitorSettings.GetNetworkInstanceName(systemName));
            _changeSystemBackgroundWorker.RunWorkerAsync();
        }

        internal void SetProgress(int percentage) => toolStripProgressBar.Value = percentage;

        private void HideProgressBarAndLabel()
        {
            toolStripStatusLabel.Visible = false;
            toolStripProgressBar.Visible = false;
        }

        internal void ChangeSystemBackgroundWorkerComplete(string systemName, IEnumerable<string> diskInstanceNames, IEnumerable<string> networkInstanceNames, CPUMonitor cpuMonitor, MemoryMonitor memoryMonitor, DiskMonitor diskMonitor, NetworkMonitor networkMonitor)
        {
            RemoteSystemMonitorSettings.SetSystem(systemName, diskMonitor.GetInstanceName(), networkMonitor.GetInstanceName());

            Logging.SetAllMonitorStartTimes();

            _updateChartBackgroundWorker.CancelIfNotNullAndBusy();

            ClearAllCharts();

            ((ToolStripMenuItem)menuStrip.Items[Constant.DiskToolStripMenuItem]).DropDownItems.Clear();
            ((ToolStripMenuItem)menuStrip.Items[Constant.NetworkToolStripMenuItem]).DropDownItems.Clear();

            ((ToolStripMenuItem)menuStrip.Items[Constant.DiskToolStripMenuItem]).DropDownItems
                .AddRange(diskInstanceNames.Select(x => new ToolStripRadioButtonMenuItem(x, diskRadioButton_OnCheckedChanged, x == diskMonitor.GetInstanceName()))
                .ToArray());

            ((ToolStripMenuItem)menuStrip.Items[Constant.NetworkToolStripMenuItem]).DropDownItems
                .AddRange(networkInstanceNames.Select(x => new ToolStripRadioButtonMenuItem(x, networkRadioButton_OnCheckedChanged, x == networkMonitor.GetInstanceName()))
                .ToArray());

            ((ToolStripMenuItem)menuStrip.Items[Constant.DiskToolStripMenuItem]).Visible = true;
            ((ToolStripMenuItem)menuStrip.Items[Constant.NetworkToolStripMenuItem]).Visible = true;

            ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).Text = systemName;

            SystemMonitor.SetMonitors(cpuMonitor, memoryMonitor, diskMonitor, networkMonitor);

            var totalMem = Math.Round(SystemMonitor.GetTotalMemoryInGigabytes());

            chart.ChartAreas[Constant.MemoryChartArea].AxisY.Maximum = totalMem;
            chart.ChartAreas[Constant.MemoryChartArea].AxisY.Interval = totalMem / 2;

            _updateChartBackgroundWorker = new UpdateChartBackgroundWorker(this);
            _updateChartBackgroundWorker.RunWorkerAsync();

            _delayedActionBackgroundWorker = new DelayedActionBackgroundWorker(() => HideProgressBarAndLabel(), 2000);
            _delayedActionBackgroundWorker.RunWorkerAsync();
        }

        internal void ChangeSystemBackgroundWorkerError(string message)
        {
            toolStripStatusLabel.Text = message;
            toolStripStatusLabel.Visible = true;
            toolStripProgressBar.Visible = false;

            var systemRadioButtons = ((ToolStripMenuItem)menuStrip.Items[Constant.SystemsToolStripMenuItem]).DropDownItems;
            var previouslySelectedIndex = systemRadioButtons.IndexOfKey(RemoteSystemMonitorSettings.GetSystemName());
            ((ToolStripRadioButtonMenuItem)systemRadioButtons[previouslySelectedIndex]).Checked = true;

            ((ToolStripMenuItem)menuStrip.Items[Constant.DiskToolStripMenuItem]).Visible = true;
            ((ToolStripMenuItem)menuStrip.Items[Constant.NetworkToolStripMenuItem]).Visible = true;

            _delayedActionBackgroundWorker = new DelayedActionBackgroundWorker(() => HideProgressBarAndLabel(), 10000);
            _delayedActionBackgroundWorker.RunWorkerAsync();
        }

        private void SetDiskInstance(string diskInstanceName)
        {
            _changeDiskInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeDiskInstanceBackgroundWorker = new ChangeDiskInstanceBackgroundWorker(this, RemoteSystemMonitorSettings.GetSystemName(), diskInstanceName);
            _changeDiskInstanceBackgroundWorker.RunWorkerAsync();
        }

        internal void ChangeDiskInstanceBackgroundWorkerComplete(DiskMonitor diskMonitor)
        {
            RemoteSystemMonitorSettings.SetDiskInstanceName(diskMonitor.GetInstanceName());

            Logging.SetDiskMonitorStartTime();

            _updateChartBackgroundWorker.CancelIfNotNullAndBusy();

            ClearDiskChart();

            SystemMonitor.SetDiskMonitor(diskMonitor);

            _updateChartBackgroundWorker = new UpdateChartBackgroundWorker(this);
            _updateChartBackgroundWorker.RunWorkerAsync();
        }

        private void SetNetworkInstance(string networkInstanceName)
        {
            _changeNetworkInstanceBackgroundWorker.CancelIfNotNullAndBusy();
            _changeNetworkInstanceBackgroundWorker = new ChangeNetworkInstanceBackgroundWorker(this, RemoteSystemMonitorSettings.GetSystemName(), networkInstanceName);
            _changeNetworkInstanceBackgroundWorker.RunWorkerAsync();
        }

        internal void ChangeNetworkInstanceBackgroundWorkerComplete(NetworkMonitor networkMonitor)
        {
            RemoteSystemMonitorSettings.SetNetworkInstanceName(networkMonitor.GetInstanceName());

            Logging.SetNetworkMonitorStartTime();

            _updateChartBackgroundWorker.CancelIfNotNullAndBusy();

            ClearNetworkChart();

            SystemMonitor.SetNetworkMonitor(networkMonitor);

            _updateChartBackgroundWorker = new UpdateChartBackgroundWorker(this);
            _updateChartBackgroundWorker.RunWorkerAsync();
        }

        internal void AddDataToChart(SystemData systemData)
        {
            AddDataPointToSeries(Constant.CPUTotalSeries, systemData.CPUData.TotalPercent);

            AddDataPointToSeries(Constant.MemoryTotalSeries, systemData.MemoryData.TotalInGigabytes);

            AddDataPointToSeries(Constant.DiskReadSeries, systemData.DiskData.ReadInMegabytesPerSecond);
            AddDataPointToSeries(Constant.DiskWriteSeries, systemData.DiskData.WriteInMegabytesPerSecond);

            AddDataPointToSeries(Constant.NetworkReceiveSeries, systemData.NetworkData.ReceiveInMegabytesPerSecond);
            AddDataPointToSeries(Constant.NetworkSendSeries, systemData.NetworkData.SendInMegabytesPerSecond);

            chart.ChartAreas[Constant.DiskChartArea].RecalculateAxesScale();
            chart.ChartAreas[Constant.NetworkChartArea].RecalculateAxesScale();
        }

        private void AddDataPointToSeries(string series, float dataPoint)
        {
            chart.Series[series].Points.InsertY(0, dataPoint);

            while (chart.Series[series].Points.Count > 61)
            {
                chart.Series[series].Points.RemoveAt(61);
            }
        }

        private void ClearAllCharts()
        {
            chart.Series[Constant.CPUTotalSeries].Points.Clear();
            chart.Series[Constant.MemoryTotalSeries].Points.Clear();

            ClearDiskChart();
            ClearNetworkChart();
        }

        private void ClearDiskChart()
        {
            chart.Series[Constant.DiskReadSeries].Points.Clear();
            chart.Series[Constant.DiskWriteSeries].Points.Clear();
        }

        private void ClearNetworkChart()
        {
            chart.Series[Constant.NetworkReceiveSeries].Points.Clear();
            chart.Series[Constant.NetworkSendSeries].Points.Clear();
        }
    }
}
