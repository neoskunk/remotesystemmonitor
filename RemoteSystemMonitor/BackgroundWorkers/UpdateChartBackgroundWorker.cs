﻿using RemoteSystemMonitor.Models;
using RemoteSystemMonitor.Monitors;
using System.ComponentModel;
using System.Threading;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal class UpdateChartBackgroundWorker : BackgroundWorker
    {
        private readonly MainForm _mainForm;

        internal UpdateChartBackgroundWorker(MainForm mainForm)
        {
            _mainForm = mainForm;

            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;

            DoWork += UpdateChartBackgroundWorker_DoWork;
            ProgressChanged += UpdateChartBackgroundWorker_ProgressChanged;
            RunWorkerCompleted += UpdateChartBackgroundWorker_RunWorkerCompleted;
        }

        private void UpdateChartBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!CancellationPending)
            {
                Thread.Sleep(1000);

                ReportProgress(0, SystemMonitor.GetSystemData());
            }
        }

        private void UpdateChartBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!CancellationPending)
            {
                _mainForm.AddDataToChart(e.UserState as SystemData);
            }
        }

        private void UpdateChartBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logging.WriteExceptionToEventLog(GetType(), e.Error);
            }
        }
    }
}
