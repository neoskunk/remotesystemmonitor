﻿using System.ComponentModel;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal static class BackgroundWorkerExtensionMethods
    {
        internal static void CancelIfNotNullAndBusy(this BackgroundWorker worker)
        {
            if (worker != null && worker.IsBusy)
            {
                worker.CancelAsync();
            }
        }
    }
}
