﻿using System;
using System.ComponentModel;
using System.Threading;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal class DelayedActionBackgroundWorker : BackgroundWorker
    {
        private readonly Action _action;
        private readonly int _milliseconds;

        internal DelayedActionBackgroundWorker(Action action, int milliseconds)
        {
            _action = action;
            _milliseconds = milliseconds;

            WorkerSupportsCancellation = true;

            DoWork += DelayedActionBackgroundWorker_DoWork;
            RunWorkerCompleted += DelayedActionBackgroundWorker_RunWorkerCompleted;
        }

        private void DelayedActionBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!CancellationPending)
            {
                Thread.Sleep(_milliseconds);
            }

            if (CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void DelayedActionBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logging.WriteExceptionToEventLog(GetType(), e.Error);
            }

            if (!e.Cancelled)
            {
                _action();
            }
        }
    }
}
