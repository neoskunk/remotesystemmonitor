﻿using RemoteSystemMonitor.Monitors;
using System.ComponentModel;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal class ChangeNetworkInstanceBackgroundWorker : BackgroundWorker
    {
        private readonly MainForm _mainForm;
        private readonly string _systemName, _networkInstanceName;

        private NetworkMonitor _networkMonitor;

        internal ChangeNetworkInstanceBackgroundWorker(MainForm mainForm, string systemName, string networkInstanceName)
        {
            _mainForm = mainForm;
            _systemName = systemName;
            _networkInstanceName = networkInstanceName;

            WorkerSupportsCancellation = true;

            DoWork += ChangeNetworkInstanceBackgroundWorker_DoWork;
            RunWorkerCompleted += ChangeNetworkInstanceBackgroundWorker_RunWorkerCompleted;
        }

        private void ChangeNetworkInstanceBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!CancellationPending)
            {
                _networkMonitor = new NetworkMonitor(_systemName, _networkInstanceName);
            }

            if (CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void ChangeNetworkInstanceBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logging.WriteExceptionToEventLog(GetType(), e.Error);
            }

            if (!e.Cancelled)
            {
                _mainForm.ChangeNetworkInstanceBackgroundWorkerComplete(_networkMonitor);
            }
        }
    }
}
