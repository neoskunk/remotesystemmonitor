﻿using RemoteSystemMonitor.Monitors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal class ChangeSystemBackgroundWorker : BackgroundWorker
    {
        private readonly MainForm _mainForm;
        private readonly string _systemName, _diskInstanceName, _networkInstanceName;

        private IEnumerable<string> _diskInstanceNames, _networkInstanceNames;
        private CPUMonitor _cpuMonitor;
        private MemoryMonitor _memoryMonitor;
        private DiskMonitor _diskMonitor;
        private NetworkMonitor _networkMonitor;

        internal ChangeSystemBackgroundWorker(MainForm mainForm, string systemName, string diskInstanceName, string networkInstanceName)
        {
            _mainForm = mainForm;
            _systemName = systemName;
            _diskInstanceName = diskInstanceName;
            _networkInstanceName = networkInstanceName;

            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;

            DoWork += ChangeSystemBackgroundWorker_DoWork;
            ProgressChanged += ChangeSystemBackgroundWorker_ProgressChanged;
            RunWorkerCompleted += ChangeSystemBackgroundWorker_RunWorkerCompleted;
        }

        private void ChangeSystemBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ReportProgress(0);

            if (!CancellationPending)
            {
                _diskInstanceNames = PerformanceCounterCategory.GetCategories(_systemName).First(x => x.CategoryName == Constant.DiskCategoryName).GetInstanceNames().OrderBy(x => x == Constant._Total ? 0 : 1).ThenBy(x => x).ToArray();

                ReportProgress((int)Math.Round(1f / 6f * 100f));

                if (!CancellationPending)
                {
                    _networkInstanceNames = PerformanceCounterCategory.GetCategories(_systemName).First(x => x.CategoryName == Constant.NetworkCategoryName).GetInstanceNames().OrderBy(x => x);

                    ReportProgress((int)Math.Round(2f / 6f * 100f));

                    if (!CancellationPending)
                    {
                        _cpuMonitor = new CPUMonitor(_systemName);

                        ReportProgress((int)Math.Round(3f / 6f * 100f));

                        if (!CancellationPending)
                        {
                            _memoryMonitor = new MemoryMonitor(_systemName);

                            ReportProgress((int)Math.Round(4f / 6f * 100f));

                            if (!CancellationPending)
                            {
                                _diskMonitor = new DiskMonitor(_systemName, _diskInstanceNames.Contains(_diskInstanceName) ? _diskInstanceName : _diskInstanceNames.First());

                                ReportProgress((int)Math.Round(5f / 6f * 100f));

                                if (!CancellationPending)
                                {
                                    _networkMonitor = new NetworkMonitor(_systemName, _networkInstanceNames.Contains(_networkInstanceName) ? _networkInstanceName : _networkInstanceNames.First());

                                    ReportProgress(100);
                                }
                            }
                        }
                    }
                }
            }

            if (CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void ChangeSystemBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) => _mainForm.SetProgress(e.ProgressPercentage);

        private void ChangeSystemBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logging.WriteExceptionToEventLog(GetType(), e.Error);

                if (!e.Cancelled)
                {
                    _mainForm.ChangeSystemBackgroundWorkerError(e.Error.Message);
                }
            }
            else if (!e.Cancelled)
            {
                _mainForm.ChangeSystemBackgroundWorkerComplete(_systemName, _diskInstanceNames, _networkInstanceNames, _cpuMonitor, _memoryMonitor, _diskMonitor, _networkMonitor);
            }
        }
    }
}
