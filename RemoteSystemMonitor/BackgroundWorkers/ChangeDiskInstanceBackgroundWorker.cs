﻿using RemoteSystemMonitor.Monitors;
using System.ComponentModel;

namespace RemoteSystemMonitor.BackgroundWorkers
{
    internal class ChangeDiskInstanceBackgroundWorker : BackgroundWorker
    {
        private readonly MainForm _mainForm;
        private readonly string _systemName, _diskInstanceName;

        private DiskMonitor _diskMonitor;

        internal ChangeDiskInstanceBackgroundWorker(MainForm mainForm, string systemName, string diskInstanceName)
        {
            _mainForm = mainForm;
            _systemName = systemName;
            _diskInstanceName = diskInstanceName;

            WorkerSupportsCancellation = true;

            DoWork += ChangeDiskInstanceBackgroundWorker_DoWork;
            RunWorkerCompleted += ChangeDiskInstanceBackgroundWorker_RunWorkerCompleted;
        }

        private void ChangeDiskInstanceBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!CancellationPending)
            {
                _diskMonitor = new DiskMonitor(_systemName, _diskInstanceName);
            }

            if (CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void ChangeDiskInstanceBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logging.WriteExceptionToEventLog(GetType(), e.Error);
            }

            if (!e.Cancelled)
            {
                _mainForm.ChangeDiskInstanceBackgroundWorkerComplete(_diskMonitor);
            }
        }
    }
}
